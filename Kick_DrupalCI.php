<?php
/**
 * This is used to kick of a DrupalCI job for Core.  This will be used at somepoint for testing contrib as well
 * For now we are just replicating the kick_pifr script.
 * You will need to get the secret from an infra team member.
 * To run,
 * 1) check options, then invoke with php 5.5+
 * 2) php Kick_DrupalCI.php -m me@example.com -k SECRET -s https://www.drupal.org/files/issues/ -f myfile.patch
 *
 */


class Kick_DrupalCI {
  /**
   * @var string
   */
  private $secret;

  /**
   * @var string
   */
  private $branch;

  /**
   * @var string
   */
  private $filename;

  /**
   * @var string
   */
  private $link;

  /**
   * @var string
   */
  private $mail;

  /**
   * @var string
   */
  private $subject;

  /**
   * @var string
   */
  private $PhpVersion;

  /**
   * Kick_DrupalCI constructor.
   *
   * @param string $secret
   *   Jenkins token.
   * @param $link
   *   URL to download the patch.
   * @param string $mail
   *   User's email address.
   * @param string $subject
   *   Subject line for email so you can describe the patch.
   *   If NULL, the filename extracted from $link will be used.
   * @param string $branch
   *   Optional branch name.
   */
  function __construct($secret, $link, $mail, $subject = NULL, $branch = '7.x', $php = NULL) {
    $this->secret = $secret;
    $this->branch = $branch;
    $this->filename = basename(parse_url($link, PHP_URL_PATH));
    $this->link = $link;
    $this->mail = $mail;

    if (!$php) {
      // Default to PHP 5.5 for 7.x tests.
      $php = strpos($branch, '7.') === 0  ? '5.5' : '7.2';
    }
    $subject = isset($subject) ? $subject : basename($this->filename, '.patch');
    $this->subject = "$subject (on $branch using PHP $php)";
    $this->PhpVersion = $this->phpVersion($php);
  }

  function buildUrl() {
    $params = array();
    $mail = filter_var($this->mail, FILTER_VALIDATE_EMAIL);
    if (!$mail) {
      throw new \InvalidArgumentException('Invalid email: ' . $this->mail);
    }
    $params['EMAIL'] = $mail;
    $params['SUBJECT'] = $this->subject;
    $params['DCI_CoreBranch'] = $this->branch; //could also be 8.0.x
    $params['DCI_DBVersion'] = "mysql-5.5"; // do not change this unless you know what value to put in
    $params['DCI_PHPVersion'] = $this->PhpVersion; // do not change this unless you know what value to put in
    //Should not need to edit things down here.
    if (preg_match('/^7\./',$params['DCI_CoreBranch'])) {
      $params['DCI_RunScript'] = '/var/www/html/scripts/run-tests.sh';
      $params['DCI_JobType'] = 'legacydevelopment';
    }
    else {
      $params['DCI_RunScript'] = '/var/www/html/core/scripts/run-tests.sh';
      $params['DCI_JobType'] = 'development';
    }
    $server = "https://dispatcher.drupalci.org/";
    $job = "DEPRECATED_security_testing";
    $link = filter_var($this->link, FILTER_VALIDATE_URL);
    if (!$link) {
      throw new \InvalidArgumentException('Invalid file server URL: ' . $this->fileserver);
    }
    $params['DCI_Fetch'] = $link . ",.";
    $params['DCI_Patch'] = $this->filename . ",.";
    $params['token'] = $this->secret;
    $params['job'] = $job;
    $url = $server . "buildByToken/buildWithParameters";

    $url .= '?' . http_build_query($params);
    return $url;
  }

  function kick() {
    $url = $this->buildUrl();
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    $result = new stdClass();
    $result->data = curl_exec($ch);
    $result->info = curl_getinfo($ch);
    if (curl_errno($ch) !== 0) {
      $result->data .= curl_error($ch);
    }
    curl_close($ch);
    return $result;
  }

  /**
   * @param string $php
   *   A PHP version like '5.5' or '7.1'
   *
   * @return string
   *   A DCI_PHPVersion constant for DrupalCI.
   */
  function phpVersion($php) {
    switch ($php) {
      case '5.3':
        return 'php-5.3.29-apache:production';
      case '5.4':
        return 'php-5.4.45-apache:production';
      case '5.5':
        return 'php-5.5.38-apache:production';
      default:
        return 'php-' . $php . '-apache:production';
    }
  }
}

// Only do this when directly invoked using cli
if (php_sapi_name() == 'cli' && !empty($_SERVER['argv']) && __FILE__ === realpath($argv[0])) {
  $shortopts = array(
    'm' => "\e[4memail\e[0m\tYour email address.",
    'k' => "\e[4msecret\e[0m\tSecret key (Jenkins token).",
    'l' => "\e[4mweburl\e[0m\tFull URL to the patch file on the web.",
    's' => "\e[4msubject\e[0m\t(Optional) Subject line for email so you can describe the patch (surround with quotes). Defaults to patch filename + branch + php version.",
    'b' => "\e[4mbranch\e[0m\t(Optional) Core branch - '7.x' or '8.0.x', defaults to '7.x'.",
    'p' => "\e[4mphp\e[0m\t(Optional) PHP Version - for example '5.5', '7.1', or '7.2'..., defaults to '5.5' for '7.x' and '7.2' for '8.0.x'.",
    'n' => "\t\tDry run flag - just print the generated URL.",
  );
  if ($argc == 1) {
    echo "Available options:\n";
    foreach($shortopts as $key => $desc) {
      echo "-$key $desc\n";
    }
    echo "\n";
    exit(1);
  }
  $longopts  = array();
  $options = array_filter(getopt(implode(':', array_keys($shortopts)), $longopts));
  $options += array('s' => NULL, 'b' => '7.x', 'n' => NULL, 'p' => NULL);
  $valid = TRUE;
  foreach ($shortopts as $key => $desc) {
    if (!array_key_exists($key, $options)) {
      if ($valid) {
        echo "Mising required options:\n";
        $valid = FALSE;
      }
      echo "-$key $desc\n";
    }
  }
  if (!$valid) {
    echo "\n";
    exit(1);
  }
  $ci = new Kick_DrupalCI($options['k'], $options['l'], $options['m'], $options['s'], $options['b'], $options['p']);
  if (isset($options['n'])) {
    echo $ci->buildUrl() . "\n";
  }
  else {
    $result = $ci->kick();
    echo $result->data;
  }
}
