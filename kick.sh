#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
token_file="$DIR"/.token
if [ -a "$token_file" ] ; then
  token=`cat "$token_file"`
fi
email_file="$DIR"/.email
if [ -a "$email_file" ] ; then
  email=`cat "$email_file"`
fi

if [ -z "$token" ] ; then
  echo -e "Missing secret token. Contact a security member for the token and then place it in a private file located at:\n $token_file"
  exit
fi

if [ -z "$email" ] ; then
  echo -e "Missing email. Place the default email address in a file located at:\n $email_file"
  exit
fi

echo -e "Enter the branch (e.g. 8.7.x) or tag (e.g. 8.6.1):"
read b
echo -e "Enter the one-time download link for the patch:"
read l
echo -e "Enter the email subject (optional):"
read s
echo -e "Enter the PHP version (optional, e.g. 5.5, 7.0, or 7.1). Defaults to 7.2 (for 8.x.y) or 5.5 (for 7.x):"
read p

php Kick_DrupalCI.php -m "$email" -k "$token" -l "$l"  -b "$b" -s "$s" -p "$p"
